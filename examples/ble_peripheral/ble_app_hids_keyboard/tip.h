#pragma once

#include "holler.h"

namespace magicwand
{
	class Tip
	{
	public:
		Tip(const Holler& holler);

		void activate();

		~Tip();

	private:
		Holler m_holler;
	};
}