#pragma once

#include <stdint.h>

namespace magicwand
{
	class Holler
	{
	public:
		Holler(uint32_t pin);

		void shout();
		void hush();

		~Holler();

	private:
		uint32_t m_pin;
	};
}