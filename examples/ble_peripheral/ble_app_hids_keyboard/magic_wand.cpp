#include "magic_wand.h"

#include <string.h>

#include "nrf_delay.h"

#include "holler.h"
#include "phalanx.h"
#include "sense.h"
#include "tip.h"

namespace magicwand
{
namespace limb
{
namespace phalanxes
{
	const size_t index_count = 1;
	const uint32_t index[index_count] = {24};
	const size_t thumb_count = 1;
	const uint32_t thumb[thumb_count] = {25};
}

namespace tips
{
	const size_t count = 2;
	const uint32_t thumb = 22;
	const uint32_t index = 23;
	const uint32_t pins[count] = {thumb, index};
}
}

	uint8_t theKey = 0;
	
	void scan()
	{
		for (uint32_t i = 0; i < limb::tips::count; i++)
		{
			Holler holler(limb::tips::pins[i]);
			holler.shout();

			Sense sense1(limb::phalanxes::index[0]);
			Sense sense2(limb::phalanxes::thumb[0]);
			nrf_delay_ms(10);
			if (sense1.hasTouch())
				theKey += i * 2 + 1;
			if (sense2.hasTouch())
				theKey += i * 2 + 2;
		}
	}

	bool hasKey()
	{
		theKey = 0x03;
		scan();
		return theKey >= 0x04;
	}

	uint8_t getKey()
	{
		return theKey;
	}
}

#include "nrf_gpio.h"

extern "C"
{
	uint8_t magic_wand_key()
	{
		if (!magicwand::hasKey())
			return 0x00;

		return magicwand::getKey();
//		nrf_delay_ms(1000);
		/*nrf_gpio_cfg_input(22, NRF_GPIO_PIN_PULLUP);
		nrf_gpio_cfg_output(23);
		nrf_gpio_pin_clear(23);
        return nrf_gpio_pin_read(22);*/
	}
}