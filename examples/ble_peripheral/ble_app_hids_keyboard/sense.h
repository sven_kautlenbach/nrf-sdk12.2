#pragma once

#include <stdint.h>

namespace magicwand
{
	class Sense
	{
	public:
		Sense(uint32_t pin);

		bool hasTouch();

		~Sense();

	private:
		uint32_t m_pin;
	};
}