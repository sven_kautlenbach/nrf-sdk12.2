#include "tip.h"

namespace magicwand
{
	Tip::Tip(const Holler& holler): m_holler(holler)
	{
	}

	void Tip::activate()
	{
		m_holler.shout();
	}

	Tip::~Tip()
	{
		m_holler.hush();
	}
}