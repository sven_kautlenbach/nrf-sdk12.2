#pragma once

#include "sense.h"

namespace magicwand
{
	class Phalanx
	{
	public:
		Phalanx(const Sense& sense);

		bool feelsTouch();

		~Phalanx();

	private:
		Sense m_sense;
	};
}