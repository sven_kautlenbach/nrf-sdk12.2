#pragma once

#include <stdint.h>

#ifdef __cplusplus
extern "C"
{
#endif
uint8_t magic_wand_key();
#ifdef __cplusplus
}
#endif