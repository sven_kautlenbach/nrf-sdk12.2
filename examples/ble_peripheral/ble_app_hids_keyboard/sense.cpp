#include "sense.h"

#include "nrf_gpio.h"

namespace magicwand
{
	Sense::Sense(uint32_t pin): m_pin(pin)
	{
		nrf_gpio_cfg_input(m_pin, NRF_GPIO_PIN_PULLUP);
	}

	bool Sense::hasTouch()
	{
		return nrf_gpio_pin_read(m_pin) == 0;
	}

	Sense::~Sense()
	{
		nrf_gpio_cfg_default(m_pin);
	}
}