#include "holler.h"

#include "nrf_gpio.h"

namespace magicwand
{
	Holler::Holler(uint32_t pin) : m_pin(pin)
	{
		hush();
	}

	void Holler::shout()
	{
    	nrf_gpio_cfg_output(m_pin);
		nrf_gpio_pin_clear(m_pin);
	}

	void Holler::hush()
	{
		nrf_gpio_cfg_default(m_pin);
	}

	Holler::~Holler()
	{
		hush();
	}
}