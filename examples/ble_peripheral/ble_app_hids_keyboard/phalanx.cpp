#include "phalanx.h"

namespace magicwand
{
	Phalanx::Phalanx(const Sense& sense): m_sense(sense)
	{
	}

	bool Phalanx::feelsTouch()
	{
		return m_sense.hasTouch();
	}

	Phalanx::~Phalanx()
	{
	}
}



/*#define PHALANX_COUNT	4

static uint32_t phalanx_pins[PHALANX_COUNT] = {22, 23, 24, 25};
static uint32_t active_count = 0;
static uint32_t last_active = 0;

void phalanx_scan()
{
	for (uint32_t i = 0; i < PHALANX_COUNT; i++)
	{
		listener(i);
		nrf_delay_ms(1);
		for (uint32_t j = 0; j < PHALANX_COUNT; i++)
		{
			if (i == j)
				continue;

			if (listener_has_contact(i))
			{
				last_active = i;
			}
		}
		listener_close(i);
	}
}

char phalanx_letter_get()
{
	if (active_count == 0)
		return 0;

	char yo = 'a';
	for (uint32_t i = 0; i < PHALANX_COUNT; i++)
		if (phalanx_pins[i] == last_active)
			return yo;
		else
			yo++;

	return 0;
}*/